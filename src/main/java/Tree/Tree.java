package Tree;

class Node {
  int val;
  Node left; Node right;
  Node(int val) {
    this.val = val;
  }
}

public class Tree {
  public int treeDiameter = Integer.MIN_VALUE;

  /**
   * @param root
   * @description: get height of tree only
   */
  int heightOfTree(Node root) {
    if (root == null) return 0;
    int lHeight = heightOfTree(root.left);
    int rHeight = heightOfTree(root.right);
    return 1 + Math.max(lHeight, rHeight);
  }

  /**
   * @param root
   * @description: get tree diameter using height
   */
  int height(Node root) {
    if (root == null) return 0;
    int lHeight = height(root.left);
    int rHeight = height(root.right);
    treeDiameter = Math.max(lHeight+rHeight+1, treeDiameter);
    return 1 + Math.max(lHeight, rHeight);
  }


}
